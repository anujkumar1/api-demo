﻿
var app = angular.module("myApp", []);

app.controller("indexCtrl", function ($scope, $http) {


    // Get function ......

    //$scope.name = 'Anuj';
    //$scope.customers = [];
    $scope.LoadLst = function () {
        var request = {
            method: 'POST',
            url: ' /cust/get-customers',
            headers: {
                'Content-Type': 'application/json'
            },
           
        }
        $http(request).then(function (x) {
            console.log(x);
            if (x.data.status == "Success") {
                $scope.customers = x.data.data;
                //console.log("customers", $scope.customers);
            }
            else {
                alert(x.data.message);
            }

        }, function () {

            alert('Some error occurred.')
        });
    };

    $scope.LoadLst();

    // Add function ......
    $scope.InsertButtonClick = function () {

        var req = {
            method: 'POST',
            url: ' /cust/add-customers',
            headers: {
                'Content-Type': 'application/json'
            },
            data: { Name: $scope.Name, City: $scope.City, Country: $scope.Country, MobileNo: $scope.MobileNo, DOJ: $scope.DOJ, Email: $scope.Email }
        };
        $http(req).then(function add(x) {
            //console.log(JSON.stringify(x));
            if (x.data.status == "Success") {
                //alert("added");
                $scope.Name = "";
                $scope.City = "";
                $scope.Country = "";
                $scope.MobileNo = "";
                $scope.DOJ = "";
                $scope.Email = "";
                $("#AddCustomerModal").modal('hide');
                $scope.LoadLst();
            }
            else {
                alert("Error => "  + x.data.message);
            }
        }, function () {
            alert('Some error occurred.')
        });
    };

    $scope.Change2FAStatus = function (record, flag) {
        var req = {
            method: 'POST',
            url: ' /cust/change2FAStatus',
            data: { ID: record.id ,Flag: flag }
        }
        $http(req).then(function add(x) {
            //console.log(JSON.stringify(x));
            if (x.data.status == "Success") {
                record.G2FAEnabled = flag
            }
            else {
                record.G2FAEnabled != flag
            }
        });
    }


    // Delete function
    $scope.DeleteButtonClick = function (x) {
        //alert("delete" + JSON.stringify(x));
        var deleterecords = {
            method: 'POST',
            url: ' /cust/delete-customers',
            headers: {
                'Content-Type': 'application/json'
            },
            data: { ID: x.id }
          
        }
        $http(deleterecords).then(function Delete(x) {
            //console.log(JSON.stringify(x));
            if (x.data.status == "Success") {
                
               $scope.LoadLst();
            }
        });
    }

    // Edit functtion....

    $scope.LoadLst();

    $scope.Edit = function (index, Data) {
       
        $("#" + index + "row #data1").text("");
        $("#" + index + "row #data2").text("");
        $("#" + index + "row #data3").text("");
        $("#" + index + "row #data4").text("");
        $("#" + index + "row #data5").text("");
        $("#" + index + "row #data6").text("");
        $("#" + index + "row #data7").text("");
        $("#" + index + "row #data1").append('<input id="myid1" type="text"  value="' + Data.name + '" class="form-control"/>');
        $("#" + index + "row #data2").append('<input id="myid2" type="text" value="' + Data.city + '" class="form-control"/>');
        $("#" + index + "row #data3").append('<input id="myid3" type="text" value="' + Data.country + '" class="form-control"/>');
        $("#" + index + "row #data4").append('<input id="myid4" type="text"  value="' + Data.MobileNo + '" class="form-control"/>');
        $("#" + index + "row #data5").append('<input id="myid5" type="text" value="' + Data.DOJ + '" class="form-control"/>');
        $("#" + index + "row #data6").append('<input id="myid6" type="text" value="' + Data.Email + '" class="form-control"/>');
        $("#" + index + "row #data7").append('<input id="myid7" type="text" value="' + Data.G2FACode + '" class="form-control"/>');
        $("#" + index + "row #Action2").show();
        $("#" + index + "row #Action1").hide();

        //$scope.EditData = {
        //    Holding: Data.name,
        //    Discount: Data.city,
        //    Tier: Data.country
        //}
    }

    $scope.Cancel = function (index, Data) {
        $("#" + index + "row #data1").text(Data.name);
        $("#" + index + "row #data2").text(Data.city);
        $("#" + index + "row #data3").text(Data.country);
        $("#" + index + "row #data4").text(Data.MobileNo);
        $("#" + index + "row #data5").text(Data.DOJ);
        $("#" + index + "row #data6").text(Data.Email);
        $("#" + index + "row #data7").text(Data.G2FACode);
        $("#" + index + "row #Action1").show();
        $("#" + index + "row #Action2").hide();
    }

    $scope.Update = function (id) {

        var name = $("#myid1").val();
        var city = $("#myid2").val();
        var country = $("#myid3").val();
        var MobileNo = $("#myid4").val();
        var DOJ = $("#myid5").val();
        var Email = $("#myid6").val();
        var G2FACode = $("#myid7").val();

        var jsonObj = { Name: name, City: city, Country: country, ID: id, MobileNo: MobileNo, DOJ: DOJ, Email: Email, G2FACode: G2FACode };

        var httprequest = {
            method: 'POST',
            url: ' /cust/update-customers',
            headers: {
                'Content-Type': 'application/json'
            },
            data: jsonObj
        };

        $http(httprequest).then(function Edit(x) {
            console.log(JSON.stringify(x));
            if (x.data.status == "Success") {
                $("#" + index + "row #data1").text(name);
                $("#" + index + "row #data2").text(city);
                $("#" + index + "row #data3").text(country);
                $("#" + index + "row #data4").text(MobileNo);
                $("#" + index + "row #data5").text(DOJ);
                $("#" + index + "row #data6").text(Email);
                $("#" + index + "row #data7").text(G2FACode);
                $("#" + index + "row #Action1").show();
                $("#" + index + "row #Action2").hide();
                $scope.LoadLst();
            }
            else {
                alert("Error => " + x.data.message);
            }

        }, function () {

            alert('Some error occurred.')
        });
    };

});




