﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Model
{
    public class Response
    {
        public Response(string status,string message, dynamic data)
        {
            this.status = status;
            this.message = message;
            this.data = data;
        }
        public string status { get; set; }
        public string message { get; set; }
        public dynamic data { get; set; }
    }
}