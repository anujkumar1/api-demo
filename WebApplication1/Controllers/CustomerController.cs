﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Model;
using WebApplication1.Model;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("Cust")]
    public class CustomerController : Controller
    {
        private readonly User db;
        public CustomerController(User db)
        {
            this.db = db;
        }

        [HttpPost]
        [Route("get-customers")]
        public async Task<object> GetCustomers()
        {
            try
            {
                var customers = await db.Customer.Where(x => x.Isactive).Select(x => new
                {
                    id = x.ID,
                    name = x.Name,
                    city = x.City,
                    Country = x.Country,
                    MobileNo = x.MobileNo,
                    DOJ = x.DOJ,
                    Email = x.Email,
                    G2FACode = x.G2FACode

                }).ToListAsync();


                //throw new Exception("Testing my try catch block.");

                return new Response("Success", "Customers retreived successfully.", customers);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new Response("Error", "Some error occurred.", null);
            }
        }

        public class CustomerPayload
        {
            public string Name { get; set; }
            public string City { get; set; }
            public string Country { get; set; }
            public bool IsActive { get; set; }
            public decimal MobileNo { get; set; }
            public DateTime DOJ { get; set; }
            public string Email { get; set; }
            public string G2FACode { get; set; }
        }

        [HttpPost]
        [Route("add-customers")]
        public async Task<object> AddCustomers(CustomerPayload req)
            {
            try
            {
                if (req == null)
                    return new Response("Error", "Bad request.", null);

                if (string.IsNullOrWhiteSpace(req.Name))
                    return new Response("Error", "invalid name", null);

                if (string.IsNullOrWhiteSpace(req.City))
                    return new Response("Error", "invalid City", null);

                if (string.IsNullOrWhiteSpace(req.Country))
                    return new Response("Error", "invalid Country", null);

                db.Customer.Add(new Customer
                {
                    Name = req.Name,
                    City = req.City,
                    Country = req.Country,
                    Isactive = true,
                    MobileNo = req.MobileNo,
                    DOJ = req.DOJ,
                    Email = req.Email,
                    G2FACode = req.G2FACode
                });
                await db.SaveChangesAsync();

                //throw new Exception("Testing my try catch block.");
                return new Response("Success", "Customers retreived successfully.", null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new Response("Error", "Some error occurred.", null);
            }

        }

        public class Change2FA
        {
            public long Id { get; set; }
            public bool enable2fa { get; set; }
            public bool Flag { get; set; }
        }

        [HttpPost]
        [Route("change2FAStatus")]
        public async Task<ActionResult<Response>> Change2FAStatus(Change2FA req)
        {
            try
            {
                var data = await db.Customer.FirstOrDefaultAsync(x => x.ID == req.Id);
                if (data == null)
                    return new Response("Error", "Id is not valid", null);
                data.G2FAEnabled = req.Flag;
                await db.SaveChangesAsync();
                return new Response("Success", "Customer status changed.", data);
            }
            catch (Exception)
            {
                return new Response("Error", "Customer status changed.", null);
            }
            
        }

        public class UpdateCustomerPayload
        {
            public long ID { get; set; }
            public string Name { get; set; }
            public string City { get; set; }
            public string Country { get; set; }
            public decimal MobileNo { get; set; }
            public DateTime DOJ { get; set; }
            public string Email { get; set; }
            public string G2FACode { get; set; }
        }
        [HttpPost]
        [Route("update-customers")]
        public async Task<object> UpdateCustomers(UpdateCustomerPayload req)
        {
            try
            {
                if (req == null || req.ID <= 0)
                    return new Response("Error", "Bad request.", null);

                if (string.IsNullOrWhiteSpace(req.Name))
                    return new Response("Error", "invalid name", null);

                if (string.IsNullOrWhiteSpace(req.City))
                    return new Response("Error", "invalid City", null);

                if (string.IsNullOrWhiteSpace(req.Country))
                    return new Response("Error", "invalid Country", null);

                var cust = await db.Customer.FirstOrDefaultAsync(x => x.ID == req.ID);
                if (cust == null)
                    return new Response("Error", "Invalid Customer ID.", null);


                cust.Name = req.Name;
                cust.City = req.City;
                cust.Country = req.Country;
                cust.MobileNo = req.MobileNo;
                cust.DOJ = req.DOJ;
                cust.Email = req.Email;
                cust.G2FACode = req.G2FACode;
                await db.SaveChangesAsync();
                //throw new Exception("Testing my throw catch block.");
                return new Response("Success", "Customer retreived successfully.", null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new Response("Error", "Some error occurred.", null);
            }

        }

        public class Class1
        {
            public long ID { get; set; }
            public bool IsActive { get; set; }

        }
        [HttpPost]
        [Route("change-status")]
        public async Task<object> ChangeStatus(Class1 req)
        {
            try
            {
                if (req == null || req.ID <= 0)
                    return new Response("Error", "Bad request.", null);

                var cust = await db.Customer.FirstOrDefaultAsync(x => x.ID == req.ID);
                if (cust == null)
                    return new Response("Error", "Invalid Customer ID.", null);

                cust.Isactive = req.IsActive;

                await db.SaveChangesAsync();
                return new Response("Success", "Customer status changed.", null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new Response("Error", "Some error occurred.", null);
            }
        }

        public class RespCustomerSearch
        {
            public string Name { get; set; }
            public string City { get; set; }
        }
        public class ReqObjCutomerSearch
        {
            public string country { get; set; }
        }
        [HttpPost]
        [Route("search-Customers")]

        public async Task<List<RespCustomerSearch>> SearchCustomers(ReqObjCutomerSearch req)
        {
            // commment
            var Cust = await db.Customer.Where(x => x.Country == req.country).Select(x => new RespCustomerSearch(){ Name = x.Name, City= x.City }).ToListAsync();
           
            return Cust;
        }
 
        public class DeleteCustomerPayload
        {
            public long ID { get; set; }
        }
        [HttpPost]
        [Route("delete-customers")]
        public async Task<object> DeleteteCustomers(DeleteCustomerPayload req)
        {
            if (req == null || req.ID <= 0)
                return new Response("Error", "Bad request.", null);

            var cust = await db.Customer.FirstOrDefaultAsync(x => x.ID == req.ID);
            if (cust == null)
                return new Response("Error", "Invalid Customer ID.", null);
                    
            db.Customer.Remove(cust);

            await db.SaveChangesAsync();

            return new Response("Success", "Customer removed successfully.", null);
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
