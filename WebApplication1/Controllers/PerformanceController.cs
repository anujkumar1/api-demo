﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Model;
using WebApplication1.Model;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("sys")]
    public class PerformanceController : ControllerBase
    {
        private readonly User db;
        public PerformanceController(User db)
        {
            this.db = db;
        }

        //  GET PERFORMANCE :

        [HttpPost]
        [Route("get-performance")]
        public async Task<object> GetPerformance(GetPerformancePayload req)
        {
            var performance = await db.Performance.Where(x => x.CID == req.CID).Select(x => new
            {
                id = x.ID,
                cid = x.CID,
                education = x.Education,
                marks = x.Marks,
                startYear = x.StartYear,
                lastYear = x.LastYear
            }).ToListAsync();

            return performance;
        }
        public class GetPerformancePayload
        {
            public long CID { get; set; }
            public string Education { get; set; }
            public decimal Marks { get; set; }
            public int StartYear { get; set; }
            public int LastYear { get; set; }
        }

        //  ADD PERFORMANCE :

        public class PerformancePayload
        {
            public long CID { get; set; }
            public string Education { get; set; }
            public decimal Marks { get; set; }
            public int StartYear { get; set; }
            public int LastYear { get; set; }

        }

        [HttpPost]
        [Route("add-performance")]
        public async Task<object> AddPerformance(PerformancePayload req)
        {

            if (req == null)
                return new Response("Error", "Bad request.", null);

            if (req == null || req.CID <= 0)
                return new Response("Error", "Bad request.", null);

            if (string.IsNullOrWhiteSpace(req.Education))
                return new Response("Error", "invalid education", null);

            if (req == null || req.Marks <= 0)
                return new Response("Error", "Marks is not less then to 0.", null);

            if (req == null || req.StartYear < 2015 || req.StartYear > 2021)
                return new Response("Error", "Start year should be between 2015 to 2021.", null);

            if (req == null || req.LastYear < 2017 || req.LastYear > 2030)
                return new Response("Error", "Last year shoule be between 2017 to 2030.", null);

            // Any() Condition used (data match in two table id )

            var customerExist = await db.Customer.AnyAsync(x => x.ID == req.CID);
            if (customerExist == false)
                return new Response("Error", "Customer does not exists.", null);

            db.Performance.Add(new Performance
            {
                CID = req.CID,
                Education = req.Education,
                Marks = req.Marks,
                StartYear = req.StartYear,
                LastYear = req.LastYear
            });

            await db.SaveChangesAsync();

            return new Response("Success", "performance added successfully.", null);

        }

        // Request Year--
        public class ReqObjSearchPerformance
        {
            public int LastYear { get; set; }
        }
        [HttpPost]
        [Route("search-performance")]

        public async Task<object> SearchPerformance(ReqObjSearchPerformance req)
        {
            var sys = await db.Performance.Where(x => x.LastYear <= req.LastYear).Select(x => new { education = x.Education }).ToListAsync();
            return sys;
        }

        // UPDATE PERFORMANCE :

        public class UpdatePerformancePayload
        {
            public long ID { get; set; }
            public long CID { get; set; }
            public string Education { get; set; }
            public long Marks { get; set; }
            public int StartYear { get; set; }
            public int LastYear { get; set; }
        }
        [HttpPost]
        [Route("update-performance")]
        public async Task<object> UpdatePerformance(UpdatePerformancePayload req)
        {
            if (req == null || req.ID <= 0)
                return new Response("Error", "Bad request.", null);

            if (req == null | req.CID <= 0)
                return new Response("Error", "Bad request", null);

            if (string.IsNullOrWhiteSpace(req.Education))
                return new Response("Error", "invalid Education.", null);

            if (req == null || req.Marks < 0)
                return new Response("Error", "Marks is not less then to 0.", null);

            if (req == null || req.StartYear < 2015 || req.StartYear > 2021)
                return new Response("Error", "Start year should be between 2015 to 2021.", null);

            if (req == null || req.LastYear < 2017 || req.LastYear > 2030)
                return new Response("Error", "Last year shoule be between 2017 to 2030.", null);


            var sys = await db.Performance.FirstOrDefaultAsync(x => x.ID == req.ID);
            if (sys == null)
                return new Response("Error", " Invalid performance CID.", null);

            sys.CID = req.CID;
            sys.Education = req.Education;
            sys.Marks = req.Marks;
            sys.StartYear = req.StartYear;
            sys.LastYear = req.LastYear;

            await db.SaveChangesAsync();

            return new Response("Successful", "Performance updated successfully.", null);
        }

        // Delete Performance ----- 

        public class DeletePerformancePayload
        {
            public long ID { get; set; }
        }
        [HttpPost]
        [Route("delete-performance")]

        public async Task<object> DeletePerformance(DeletePerformancePayload req)
        {
            if (req == null || req.ID <= 0)
                return new Response("Error", " Bad request", null);

            var sys = await db.Performance.FirstOrDefaultAsync(x => x.ID == req.ID);
            if (sys == null)
                return new Response("Error", "Invalid performance Id.", null);

            db.Performance.Remove(sys);

            await db.SaveChangesAsync();

            return new Response("Successful", "Performance removed successfully.", null);
        }
    }
}
