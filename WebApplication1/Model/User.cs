﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Model
{
    public class User : DbContext
    {
        public User(DbContextOptions<User> options) : base(options)
        {

        }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Performance> Performance { get; set; }
    }


    public class Customer
    {
        [Key]
        public long ID { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public bool Isactive { get; set; }
        public decimal MobileNo { get; set; }
        public DateTime DOJ { get; set; }
        public string Email { get; set; }
        public string G2FACode { get; set; }
        public bool G2FAEnabled { get; set; }

    }

    public class Performance
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID { get; set; }
        public long CID { get; set; }
        public string Education { get; set; }
        public decimal Marks { get; set; }
        public int StartYear { get; set; }
        public int LastYear { get; set; }
    }

}
