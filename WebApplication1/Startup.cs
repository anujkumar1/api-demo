using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using WebApplication1.Model;

namespace WebApplication1
{
    public class Startup
    {
         public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var MYSQL_IP = Environment.GetEnvironmentVariable("MYSQL_IP");
            var MYSQL_PORT = Environment.GetEnvironmentVariable("MYSQL_PORT");
            var MYSQL_DB = Environment.GetEnvironmentVariable("MYSQL_DB");
            var MYSQL_USER = Environment.GetEnvironmentVariable("MYSQL_USER");
            var MYSQL_PASSWORD = Environment.GetEnvironmentVariable("MYSQL_PASSWORD");

            var MYSQL_conn_STRING = $"Server={MYSQL_IP}; Database={MYSQL_DB}; Username={MYSQL_USER}; Port={MYSQL_PORT};Password={MYSQL_PASSWORD};Pooling=true;Minimum Pool Size=0;";
                
            services.AddDbContext<User>(options =>
            {
                options.UseMySql(MYSQL_conn_STRING, ServerVersion.AutoDetect(MYSQL_conn_STRING));
            });
            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

            }

            app.UseStaticFiles();

            //app.UseHttpsRedirection();

            app.UseRouting();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
